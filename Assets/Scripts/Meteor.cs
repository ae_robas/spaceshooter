﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Meteor : MonoBehaviour
{
    public GameObject [] graphics;
    int seleccionado;
    Vector2 speed;
    public AudioSource audioSource;
    public ParticleSystem explosion2;
    public GameObject meteorToInstanciate; 

    void Awake()
    {
        for (int i = 0; i < graphics.Length; i++)
        {
           graphics[i].SetActive(false);
        }

        seleccionado = Random.Range(0, graphics.Length);
        graphics[seleccionado].SetActive(true);

        speed.x = Random.Range (-3,0);
        speed.y = Random.Range (-1,5);
    }

    

    private void Update()
    {
        transform.Translate(speed *Time.deltaTime);
        graphics[seleccionado].transform.Rotate(0, 0, 100 * Time.deltaTime);
    }

    public void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Finish")
        {
            Destroy(this.gameObject);
        }
        else if (other.tag == "Bullet")
        {
            StartCoroutine(DestroyMeteor());
        }
    }

    IEnumerator DestroyMeteor()
    {
        //Desactivo el grafico
        graphics[seleccionado].SetActive(false);

        //Elimino el BoxCollider
        Destroy(GetComponent<BoxCollider2D>());

        //Lanzar particula
        explosion2.Play();

        //Lanzo sonido de explosion
        //audioSource.Play();

        //Me espero 1 segundo
        yield return new WaitForSeconds(1.0f);

        //Me destruyo a mi mismo
        Destroy(this.gameObject);
    }

    public virtual void InstanceMeteors()
    {
        Instantiate(meteorToInstanciate, this.transform.position, Quaternion.identity, null);
        Instantiate(meteorToInstanciate, this.transform.position, Quaternion.identity, null);
        Instantiate(meteorToInstanciate, this.transform.position, Quaternion.identity, null);
        Instantiate(meteorToInstanciate, this.transform.position, Quaternion.identity, null);
    }






}
