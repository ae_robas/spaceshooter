﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBehaviour : MonoBehaviour
{
    public float speed;
    private Vector2 axis;
    public Vector2 limits;    

    int seleccionado;   
    

    [SerializeField] GameObject graphics;
    [SerializeField] Collider2D collider;
    [SerializeField] ParticleSystem explosion2;
    [SerializeField] AudioSource audioSource;

    public float shootTime = 0;

    public Weapon weapon;
    public Propeller prop;

    public int lives = 3;
    private bool iamDead = false;

    private void Update()
    {
        if (iamDead)
        {
            return;
        }

        shootTime += Time.deltaTime;

        transform.Translate(axis * speed * Time.deltaTime);//movimiento nave time.deltatime -> tiempo que ha pasado entre un frame y otro
        if (transform.position.y > limits.y)
        {
            transform.position = new Vector3(transform.position.x, limits.y, transform.position.z);
        }
        else if (transform.position.y < -limits.y)
        {
            transform.position = new Vector3(transform.position.x, -limits.y, transform.position.z);
        }
        if (transform.position.x > limits.x)
        {
            transform.position = new Vector3(limits.x, transform.position.y, transform.position.z);
        }
        else if (transform.position.x < -limits.x)
        {
            transform.position = new Vector3(-limits.x, transform.position.y, transform.position.z);
        }

        if (axis.x > 0)
        {
            prop.BlueFire();
        }
        else if (axis.x < 0)
        {
            prop.RedFire();
        }
        else
        {
            prop.Stop();
        }

        /*if (axis.x <= 0)
        {
            prop.RedFire();
        }
        else
        {
            prop.Stop();
        }*/



    }

    public void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Meteor")
        {
            StartCoroutine(Destroyship());
        }

    }

    IEnumerator Destroyship()
    {

        //Indico que estoy muerto
        iamDead = true;

        //Me quito una vida
        lives--;

        //Desactivo el grafico
        graphics.SetActive(false);

        //Elimino el BoxCollider
        collider.enabled = false;

        //Lanzar particula
        explosion2.Play();

        //Lanzo sonido de explosion
        //audioSource.Play();

        //Desactivo el propeller
        prop.gameObject.SetActive(false);

        //Me espero 1 segundo
        yield return new WaitForSeconds(3.0f);

        //Miro si tengo mas vidas
        if (lives > 0)
        {
            //Vuelvo a activar el jugador
            iamDead = false;
            graphics.SetActive(true);
            collider.enabled = true;
            //Activo el propeller
            prop.gameObject.SetActive(true);
        }
    }

    IEnumerator inMortal() {
        iamDead = false;
        graphics.SetActive(true);
        prop.gameObject.SetActive(true);

        for(int i=0; i<15; i++){
            graphics.SetActive(false);
            yield return new WaitForSeconds(0.1f);
            graphics.SetActive(true);
            yield return new WaitForSeconds(0.1f);

        }
        collider.enabled = true;
    }



    public void SetAxis(Vector2 CurrentAxis)
    {
        axis = CurrentAxis;
    }


    public void Shoot()
    {
        if (shootTime > weapon.GetCadencia())
        {
            //Debug.Log("Dispara");
            weapon.Shoot();

        }

    }
}
